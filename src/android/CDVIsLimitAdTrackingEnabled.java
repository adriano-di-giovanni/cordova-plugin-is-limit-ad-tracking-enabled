package com.adrianodigiovanni.islimitadtrackingenabled;

import android.app.Activity;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class CDVIsLimitAdTrackingEnabled extends CordovaPlugin
{
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("get")) {
            final Activity activity = cordova.getActivity();
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    AdvertisingIdClient.Info adInfo = null;
                    try {
                        adInfo = AdvertisingIdClient.getAdvertisingIdInfo(activity);
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, adInfo.isLimitAdTrackingEnabled()));
                    } catch (Exception e) {
                        callbackContext.error(e.getMessage());
                    }
                }
            });
            return true;
        }
        return false;
    }
}
