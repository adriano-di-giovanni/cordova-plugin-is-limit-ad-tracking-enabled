#import <Cordova/CDV.h>

@interface CDVIsLimitAdTrackingEnabled : CDVPlugin {}

- (void)get:(CDVInvokedUrlCommand*)command;

@end
