#import "CDVIsLimitAdTrackingEnabled.h"
#import <AdSupport/AdSupport.h>
@implementation CDVIsLimitAdTrackingEnabled

- (void)get:(CDVInvokedUrlCommand*)command
{
    // NOTE: please note that result from isAdvertisingTrackingEnabled is negated.
    BOOL isLimitAdTrackingEnabled = ![[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:isLimitAdTrackingEnabled];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
