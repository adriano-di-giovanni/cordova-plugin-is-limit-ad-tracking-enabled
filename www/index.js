var exec = require('cordova/exec')

module.exports = function(successCallback, aErrorCallback) {
    var errorCallback = function(errMessage) {
        aErrorCallback(new Error(errMessage))
    }
    exec(successCallback, errorCallback, 'isLimitAdTrackingEnabled', 'get', [])
}
