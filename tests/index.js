exports.defineAutoTests = function() {
    it('should be defined', function() {
        expect(window.plugins.isLimitAdTrackingEnabled).toBeDefined()
    })

    it('should be a function', function() {
        expect(typeof window.plugins.isLimitAdTrackingEnabled).toEqual('function')
    })

    it('should asynchronously return a boolean value', function(done) {
        var successCallback = function(result) {
            expect(typeof result).toEqual('boolean')
            done()
        }
        window.plugins.isLimitAdTrackingEnabled(successCallback, done)
    })
}
